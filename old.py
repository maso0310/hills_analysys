"""
1.建立要放NDVI、NDRE結果的csv，寫入欄位名稱
2.建立要寫入csv的資料容器，是由{key:value}所組成
3.以for迴圈形式將要處理的大圖都讀取出來
4.找出大圖當中的Otsu閾值，找出植體的位置
5.把單株的shapefile炸裂，變成多個單元
6.依照炸裂後的shapefile單元，將大圖的tif檔案進行切割
7.


"""


#讀取檔案用
import os
import glob
from pathlib import Path

#分析影像用
from osgeo import gdal, ogr
from skimage import io, filters #Otsu演算法
from skimage.color import rgb2hsv, rgb2lab
import shapefile
import numpy as np 

#圖片顯示用
import matplotlib.pyplot as plt

#輸出csv用
import csv

#顯示處理進度與計時用
from tqdm import tqdm, trange
import time

#輸出csv欄位名稱
fieldnames = ['Id', 'site', 'date', 'X', 'Y', 'Field', 'GC', 'NDVI', 'NDVI_sum', 'NDVI_max', 'NDVI_min', 'NDVI_range', 'NDVI_std', 'NDRE', 'NDRE_sum', 'NDRE_max', 'NDRE_min', 'NDRE_range', 'NDRE_std']

#給定開始時間
st_time = time.time()

#建立資料夾是否存在的判斷函數
def dir_maker(dir_path):
    if os.path.isdir(dir_path) == False:
        os.mkdir(dir_path)
    else:
        pass


#判斷是否存在data與images資料夾
dir_maker(f'./03_Result/data')
dir_maker(f'./03_Result/images')

#建立輸出結果的csv
with open('./03_Result/data/Plant_Point_Analysis.csv','w',encoding='utf-8',newline='') as f:
    writer = csv.DictWriter(f,fieldnames=fieldnames)
    writer.writeheader()

#開啟csv檔案(新增資料格式)
with open('./03_Result/data/Plant_Point_Analysis.csv','a',encoding='utf-8',newline='') as f:
    writer = csv.DictWriter(f,fieldnames=fieldnames)

    #把需要分析的原圖都先load出來，標定site跟data
    for image_path in glob.glob(f'./01_TIF/*/*/*.tif'):

        #建立要寫入csv的資料容器
        data = {}

        #TIF檔名稱
        tif_name = Path(image_path).stem

        #調查場域
        site = Path(image_path).parts[-3]
        print(site)
        #調查日期
        date = tif_name[:10].replace('-','')
        print(date)

        data['site'] = site
        data['date'] = date

        #讀取tif原圖為dataset
        dataset = gdal.Open(image_path,gdal.GA_ReadOnly)

        #讀取dataset當中的座標資訊
        ds = dataset.GetGeoTransform()

        #讀取tif原圖的影像資料，並且正規化
        img  = dataset.ReadAsArray()/ 65535

        #將數值為1(即原本為65535之值)設定為0
        img[img==1.0]=0

        #讀取影像資料中的各波段，以進行植生指標運算
        B = img[0,:,:]
        G = img[1,:,:]
        R = img[2,:,:]
        RE = img[3,:,:]
        NIR = img[4,:,:]

        #計算NDVI跟NDRE               
        NDVI = (NIR - R) / (NIR + R + (10**(-10)))
        NDRE = (NIR - RE) / (NIR + RE + (10**(-10)))

        #分別使用Otsu大津演算法二值化
        thresh1 = filters.threshold_otsu(NIR)
        thresh2 = filters.threshold_otsu(NDVI)
        thresh3 = filters.threshold_otsu(NDRE)

        #建立mask
        mask1 = (NIR >= thresh1) * 1.0
        mask2 = (NDVI >= thresh2) * 1.0
        mask3 = (NDRE >= thresh3) * 1.0

        #植體遮罩 NIR + NDVI + NDRE
        mask = mask1 * mask2 * mask3

        #辨識期作別
        if int(date[:4]) == 2022:
            if int(date[4:6]) < 7:
                Period = '111-1'
            else:
                Period = '111-2'

        #把植株定位後的Shapefile讀出來，每個shapefile的polygon當作一個分析單元，以每個場域為單元
        shapefile_path = glob.glob(f'./02_Shapefile/{site}/{Period}/Buffer*.shp')[0]

        #讀取整個shapefile
        input_shapefile = ogr.Open(shapefile_path)
        input_layer = input_shapefile.GetLayer(0)

        #建立調查區域與日期的檔案
        dir_maker(f'./03_Result/images/{site}')
        dir_maker(f'./03_Result/images/{site}/{date}')

        print(f'{site} {date}切割小塊的shapefile')
        #依照shapefile裡面的數量執行迴圈內容
        for i in trange(input_layer.GetFeatureCount()):
            #處理第i個shapefile
            feature = input_layer.GetFeature(i)

            #獲得第i個shapefile的Id、X、Y、Field
            Id = feature.GetField('Id')
            X = feature.GetField('X')
            Y = feature.GetField('Y')
            Field = feature.GetField('Field')
            
            #將data當中的指定鍵值放入資料
            data['Id'] = Id
            data['X'] = X
            data['Y'] = Y
            data['Field'] = Field

            #針對每個日期每個區域，建立用shapefile切下來的檔案資料夾
            dir_maker(f'./03_Result/images/{site}/{date}/{Field}')
            dir_maker(f'./03_Result/images/{site}/{date}/{Field}/shapefile')
            dir_maker(f'./03_Result/images/{site}/{date}/{Field}/tif')

            #指定要輸出各別單株shapefile的檔案路徑位置
            shp_output_path = f'./03_Result/images/{site}/{date}/{Field}/shapefile'

            #組合儲存路徑與名稱
            save_shp_path = os.path.join(shp_output_path,(f"{Id}_{X}_{Y}_{Field}.shp"))
            #判定shapefile檔案是否已存在，若以存再則跳過
            if os.path.isfile(save_shp_path) == True:
                continue

            #建立一個ESRI Shapefile的Driver物件
            driver = ogr.GetDriverByName('ESRI Shapefile') ## a driver 

            #指定drvier物件的實體檔案建立路徑位址
            datasource = driver.CreateDataSource(save_shp_path)

            #建立圖層
            layer = datasource.CreateLayer('layerName',geom_type=ogr.wkbPolygon)

            #指定要儲存的圖層資料
            layer.CreateFeature(feature)
#            geometry = feature.GetGeometryRef()

        print(f'{site} {date}小塊的shapefile切割完畢', '花費時間',round(time.time() - st_time,2),"秒")        

        #計時&重新計時
        print(f'{site} {date}切割小塊的tif檔案')
        st_time = time.time()

        #開啟要切割的TIF檔案大圖
        input_raster = gdal.Open(image_path)

        #讀取該場域該日期的所有shapefile檔案
        shapefile_path_list = glob.glob(f'./03_Result/images/{site}/{date}/*/shapefile/*.shp')

        #建立小塊tif檔案切割進度條
        for i in trange(len(shapefile_path_list)):
            #嘗試以下動作
            try:
                #指定的shapefile路徑
                shp_path = shapefile_path_list[i]

                #樣區編號
                Field = Path(shp_path).parts[-3]

                #小塊tif檔案的輸出路徑
                tif_file_output_path = f'./03_Result/images/{site}/{date}/{Field}/tif/' + Path(shp_path).stem + '.tif'

                #如果已經輸出過就跳過換下一張
                if os.path.isfile(tif_file_output_path) == True:
                    continue

                #獲得輸入shapefile的左上右下座標資料
                r = shapefile.Reader(shp_path)
                minX, minY, maxX, maxY = r.bbox
                
                #裁切tif檔案：用每個小塊shapefile，從原始大圖中一塊一塊切出來
                ds = gdal.Warp(
                    tif_file_output_path,#輸出raster路徑
                    input_raster,#輸入raster檔案，用gdal.Open()開啟
                    format = 'GTiff',#格式
                    cutlineDSName = shp_path,#shapefile路徑
                    outputBounds = [minX, minY, maxX, maxY],
                    dstNodata = 0,
                )

                del ds
            #例外狀況發生時的動作
            except Exception as e:
                pass

        #計時&重新計時
        print(f'{site} {date}小塊tif切割結束', '花費時間',round(time.time() - st_time,2),"秒") 
        st_time = time.time()       

        #建立該場域該日期的所有樣區的tif檔案路徑list
        tif_file_path_list = glob.glob(f'./03_Result/images/{site}/{date}/*/tif/*.tif')

        #建立分析植生指標的進度條
        for i in trange(len(tif_file_path_list)):
            #指定要分析的tif檔案
            tif_file_path = tif_file_path_list[i]

            #該tif檔案名稱
            tif_name = Path(tif_file_path).stem

            #用檔案名稱把資訊讀出來
            Id = tif_name.split("_")[0]
            X = tif_name.split("_")[1]
            Y = tif_name.split("_")[2]
            Field = tif_name.split("_")[3]

            #將將資料裝入data容器當中，若原本已經有值，則直接覆蓋替換
            data['Id'] = Id
            data['X'] = X
            data['Y'] = Y
            data['Field'] = Field

            #讀取原圖tif檔的資訊
            dataset = gdal.Open(tif_file_path,gdal.GA_ReadOnly)
            ds = dataset.GetGeoTransform()

            #讀取tif檔案原圖圖片資訊
            image  = dataset.ReadAsArray()/ 65535
            image[image==1.0]=0

            #讀取所有band
            B = image[0,:,:]
            G = image[1,:,:]
            R = image[2,:,:]
            RE = image[3,:,:]
            NIR = image[4,:,:]

            #計算NDVI跟NDRE                
            NDVI = (NIR - R) / (NIR + R + (10**(-10)))
            NDRE = (NIR - RE) / (NIR + RE + (10**(-10)))

            #建立mask
            mask1 = (NIR >= thresh1) * 1.0
            mask2 = (NDVI >= thresh2) * 1.0
            mask3 = (NDRE >= thresh3) * 1.0

            #建立植體遮罩
            mask = mask1 * mask2 * mask3

            #計算冠層覆蓋率，運算邏輯：計算原圖(image)的隨便一個波段所有非零像素植體的數量，再除以原圖該波段的所有非零像素植體數量
            GC_area = np.count_nonzero(image[0,:,:] * mask)
            total_area = np.count_nonzero(image[0,:,:])
            GC_rate =(GC_area / total_area)

            #建立植生指標名稱list，用來建立圖層儲存迴圈
            VIs = ['NDVI', 'NDRE']

            #將兩個植生指標處理完畢的小圖tif儲存起來
            for VI in VIs:
                #img為植體面積與NDVI、NDRE的集合
                img = eval(VI) * mask

                #確認輸出路徑資料夾是否存在
                dir_maker(f'./03_Result/images/{site}/{date}/{Field}/{VI}')

                try:
                    #設定要儲存的tif檔案路徑，以原圖的dataset設定，並放入一個band，輸出格式為float32
                    new_dataset = gdal.Translate(f'./03_Result/images/{site}/{date}/{Field}/{VI}/{tif_name}.tif', dataset,bandList=[1],outputType=gdal.GDT_Float32) 
                    new_dataset.WriteArray(img)
                    new_dataset = None

                except:
                    pass

            #將原本的NDVI與NDRE與植體遮罩取交集，濾除不相干區域
            NDVI = NDVI * mask
            NDRE = NDRE * mask

            #將nan值都設定為0
            NDVI[np.isnan(NDVI)] = 0
            NDRE[np.isnan(NDRE)] = 0

            #指取得非0數值
            NDVI = NDVI[NDVI!=0]
            NDRE = NDRE[NDRE!=0]

            #將GC_rate作為冠層覆蓋濾放入data
            data['GC'] = GC_rate

            #計算NDVI數列的非nan平均值，如果還是nan值，則平均值與其他統計量一樣設定為0
            data['NDVI'] = np.nanmean(NDVI)

            if str(data['NDVI']) == 'nan':
                data['NDVI'] = 0
                data['NDVI_sum'] = 0
                data['NDVI_max'] = 0
                data['NDVI_min'] = 0
                data['NDVI_range'] = 0
                data['NDVI_std'] = 0

            else:
                data['NDVI_sum'] = np.nansum(NDVI)
                data['NDVI_max'] = np.nanmax(NDVI)
                data['NDVI_min'] = np.nanmin(NDVI)
                data['NDVI_range'] = np.nanmax(NDVI) - np.nanmin(NDVI)
                data['NDVI_std'] = np.nanstd(NDVI)


            #計算NDRE數列的非nan平均值，如果還是nan值，則平均值與其他統計量一樣設定為0
            data['NDRE'] = np.nanmean(NDRE)
            if str(data['NDRE']) == 'nan':
                data['NDRE'] = 0
                data['NDRE_sum'] = 0
                data['NDRE_max'] = 0
                data['NDRE_min'] = 0
                data['NDRE_range'] = 0
                data['NDRE_std'] = 0

            else:
                data['NDRE_sum'] = np.nansum(NDRE)
                data['NDRE_max'] = np.nanmax(NDRE)
                data['NDRE_min'] = np.nanmin(NDRE)
                data['NDRE_range'] = np.nanmax(NDRE) - np.nanmin(NDRE)
                data['NDRE_std'] = np.nanstd(NDRE)
            
            #將一小塊tif檔案的分析結果寫入csv檔案中
            writer.writerow(data)

    #分析完畢，計時
    print(f'{site} {date}分析結束', '花費時間',round(time.time() - st_time,2),"秒")