asgiref==3.4.1
Bottleneck==1.3.2
certifi==2021.10.8
click==8.0.3
click-plugins==1.1.1
cligj==0.7.2
cloudpickle @ file:///tmp/build/80754af9/cloudpickle_1598884132938/work
colorama==0.4.4
cycler==0.10.0
cytoolz==0.11.0
dask @ file:///tmp/build/80754af9/dask-core_1629132726831/work
Django==3.2.9
et-xmlfile==1.1.0
fonttools==4.25.0
fsspec @ file:///tmp/build/80754af9/fsspec_1626383727127/work
GDAL==3.3.1
image==1.5.33
imagecodecs==2021.7.30
imageio @ file:///tmp/build/80754af9/imageio_1617700267927/work
importlib-metadata==4.8.2
joblib==1.1.0
kiwisolver @ file:///C:/ci/kiwisolver_1612282618948/work
locket==0.2.1
matplotlib @ file:///C:/ci/matplotlib-suite_1628011511716/work
mkl-service==2.4.0
munch==2.5.0
munkres==1.1.4
networkx @ file:///tmp/build/80754af9/networkx_1627459939258/work
numexpr @ file:///C:/ci/numexpr_1618856761305/work
numpy @ file:///D:/bld/numpy_1629092219550/work
olefile==0.46
opencv-python==4.5.5.62
openpyxl==3.0.9
packaging @ file:///tmp/build/80754af9/packaging_1625611678980/work
pandas @ file:///C:/ci/pandas_1627570311072/work
partd @ file:///tmp/build/80754af9/partd_1618000087440/work
Pillow @ file:///C:/ci/pillow_1625663286921/work
pyparsing @ file:///home/linux1/recipes/ci/pyparsing_1610983426697/work
PyQt5==5.15.0
PyQt5-sip==12.9.0
pyqt5-tools==5.15.0.1.7
pyshp==2.2.0
python-dateutil @ file:///tmp/build/80754af9/python-dateutil_1626374649649/work
python-dotenv==0.14.0
pytz @ file:///tmp/build/80754af9/pytz_1612215392582/work
PyWavelets @ file:///C:/ci/pywavelets_1601658407053/work
PyYAML==5.4.1
scikit-image==0.18.1
scikit-learn==1.0.2
scipy @ file:///C:/ci/scipy_1618856134946/work
six @ file:///tmp/build/80754af9/six_1623709665295/work
sqlparse==0.4.2
threadpoolctl==3.1.0
tifffile==2020.10.1
toolz @ file:///home/linux1/recipes/ci/toolz_1610987900194/work
tornado @ file:///C:/ci/tornado_1606935947090/work
tqdm==4.64.0
typing-extensions==4.0.1
wincertstore==0.2
zipp==3.6.0
